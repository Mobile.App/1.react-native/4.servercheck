/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { version } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Dimensions,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import AutoHeightImage from 'react-native-auto-height-image';
import Animated,{Easing, Extrapolate} from 'react-native-reanimated'

const bgImg = require('./assets/bg.jpg');
const titleImg = require('./assets/title.png');
const companyLogoImg = require('./assets/ITHC_Logo.png');

const {width, height} = Dimensions.get('window');

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      animationInit: false,
      startRequest: false,
      counterData: '0',
      timeoutData: 0,
      ipAddr: '',
      stateTextInput: true,
      input2: 'green',
      input3: 'green',
      input4: 'green',
      input5: 'green',
      startRunningText: 'Start',
    }
    console.disableYellowBox = true;

    this.opacityInit = new Animated.Value(1);
    this.opacityTextInput = new Animated.Value(1);

    this.intervalCheckTimeOut = null;
  }
  componentDidMount = () => {
    console.log('componentDidMount');

    this.opacityInit.setValue(0);
    Animated.timing(
      this.opacityInit,
      {
        toValue: 1,
        duration: 1000,
        easing: Easing.linear
      }
    ).start(()=>this.hiddenLogoInit());
  }
  hiddenLogoInit(){
    setTimeout(()=>this.animationHiddenLogoInit(), 800);
  }
  animationHiddenLogoInit(){
    Animated.timing(
      this.opacityInit,
      {
        toValue: 0,
        duration: 1000,
        easing: Easing.linear
      }
    ).start(()=>this.displayMain());
  }
  displayMain(){
    this.setState({animationInit: true});
    Animated.timing(
      this.opacityInit,
      {
        toValue: 1,
        duration: 1000,
        easing: Easing.linear
      }
    ).start();
  }
  checkTimeout(){
    //this.reTry();
    console.log('Timeout--------------------')
    this.state.timeoutData++;
    clearInterval(this.intervalCheckTimeOut);
  }
  request(){
    console.log('Request----------------------');
    this.intervalCheckTimeOut = setInterval(()=>this.checkTimeout(), 3000);
    fetch('http://' + this.state.ipAddr.toString())
      .then((response) => response.text())
      .then((json) => {
        this.state.counterData++;
        console.log('Receive-------------------');
        setTimeout(()=>this.reTry(), 100);        
        var jsonStr = json.substring(1, json.length - 3);
        console.log(jsonStr);
        var obj = JSON.parse(jsonStr);
        if(obj.Input2 == 0){
          this.state.input2 = 'green';
        }
        else{
          this.state.input2 = 'red';
        }
        if(obj.Input3 == 0){
          this.state.input3 = 'green';
        }
        else{
          this.state.input3 = 'red';
        }
        if(obj.Input4 == 0){
          this.state.input4 = 'green';
        }
        else{
          this.state.input4 = 'red';
        }
        if(obj.Input5 == 0){
          this.setState({input5: 'green'});
        }
        else{
          this.setState({input5: 'red'});
        }

        clearInterval(this.intervalCheckTimeOut);
      })
      .catch((error) => {
        console.log('error-------------------');
        console.log(error);
        clearInterval(this.intervalCheckTimeOut);
        setTimeout(()=>this.reTry(), 100);
      });
  }
  reTry(){
    setTimeout(()=>this.request(), 100);
  }
  assignIp(text){
    if(!this.state.startRequest){
      this.state.ipAddr = text;
    }
  }
  startCheck(){
    if(!this.state.startRequest){
      this.state.startRequest = true;
      this.request();
      
      this.state.startRunningText = 'Running...';
      this.setState({stateTextInput: false});
      Animated.timing(
        this.opacityTextInput,
        {
          toValue: 0,
          duration: 500,
          easing: Easing.linear
        }
      ).start();
    }
  }

  render(){
    if(!this.state.animationInit){
      return(
        <Animated.View style={{opacity: this.opacityInit, backgroundColor:'#ffcb9a', justifyContent:'center', alignItems:'center', height:'100%'}}>
          <AutoHeightImage width={width/2} source={companyLogoImg} style={{}}></AutoHeightImage>
        </Animated.View>
      )
    }
    else{
      return(
        <Animated.View style={{
          opacity:this.opacityInit,
          flex: 1,
          flexDirection: 'column',
        }}>
          <Image source={bgImg} style={styles.backgroundImage}></Image>
          <View style={{flex:8}}>
            <View style={styles.title}>
              <AutoHeightImage width={width/2} source={titleImg}></AutoHeightImage>
            </View>
            <View style={styles.body}>
              <View style={{paddingTop: 100}}>
                <Text style={styles.counter}>Counter: {this.state.counterData}</Text>
                <Text style={styles.counter}>Timeout: {this.state.timeoutData}</Text>
              </View>
              <View style={{flexDirection: 'row', height: (width/4)}}>
                <View style={{flex:1, borderRadius: width/8, backgroundColor: this.state.input2, margin: width/16}}></View>
                <View style={{flex:1, borderRadius: width/8, backgroundColor: this.state.input3, margin: width/16}}></View>
                <View style={{flex:1, borderRadius: width/8, backgroundColor: this.state.input4, margin: width/16}}></View>
                <View style={{flex:1, borderRadius: width/8, backgroundColor: this.state.input5, margin: width/16}}></View>
              </View>
            </View>
          </View>
          <Animated.View style={{opacity: this.opacityTextInput,flex: 1, margin: 10, borderRadius: 20}}>
            <TextInput
              style={{ height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: '#edf5e1'}}
              onChangeText={text => this.assignIp(text)}
              editable={this.state.stateTextInput}
              
            />
          </Animated.View>
          <View style={{flex: 1, margin: 10, borderRadius: 20, backgroundColor: 'green'}}>
            <TouchableOpacity style={{flex: 1, margin: 10, borderRadius: 20, justifyContent:'center', alignItems:'center'}} onPress={()=>this.startCheck()}>
              <Text style={{fontSize: 20}}>{this.state.startRunningText}</Text>                
            </TouchableOpacity>
          </View>
        </Animated.View>
      )
    }    
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    flexDirection: 'column',
  },
  backgroundImage: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    opacity: 1,
  },
  title:{
    flex: 1,
    justifyContent: 'center',
    flexWrap: 'wrap',
    marginLeft: 20,
  },
  titleImg:{
    flex: 1,
    width: 300,
  },
  titleText:{
    textAlign: 'center',
    fontSize: 28,
    fontWeight: 'bold',
    color:'red'
  },
  body:{
    flex: 9,
    justifyContent: 'center',
  },
  counter:{
    textAlign: 'center',
    fontSize: 25,
  },
});

export default App;
